var fs = require('fs');

/**
 * @param dir {string} Folder in which we would search for Swift files
 * @param callback {(err: string, swiftFiles: string[]) => void} Callback that recieves the files
 * @return {Promise<string[]>}
 */
function allSwiftFiles(dir) {
	return new Promise((resolve, reject) => {
		fs.readdir(dir, { encoding: 'utf-8' }, (err, files) => {
			if (err) {
				reject(err);
				return;
			}
			var promises = files
				.filter((dirEnt) => { return fs.statSync(dir + "/" + dirEnt).isDirectory(); })
				.map((dirEnt) => { return allSwiftFiles(dir + "/" + dirEnt); });
			var currentSwiftFiles = files
				.filter((dirEnt) => { return fs.statSync(dir + "/" + dirEnt).isFile(); })
				.filter((dirEnt) => { return dirEnt.search("\.swift") > 0; })
				.map((dirEnt) => { return Promise.resolve([dir + "/" + dirEnt]); });
			Promise.all(promises.concat(currentSwiftFiles))
				.then((arrays) => { resolve(arrays.reduce((accumulator, currentValue) => { return accumulator.concat(currentValue); }, [])); })
				.catch((err) => { reject(err); });
		});
	});
}

exports.allSwiftFiles = allSwiftFiles;
