var fs = require('fs');
var exec = require('child_process');

/**
 * @param file Swift file to analyse
 * @return {Promise<Object>} Promise that contains the analysed code
 */
function analyse(file) {
	return new Promise((resolve, reject) => exec.exec("sourcekitten structure --file \"" + file + "\"", (err, stdout, stderr) => { 
		if(err) { reject(stderr); return; }
		resolve(JSON.parse(stdout));
	}));
}

const substructure = "key.substructure";
const inheritedTypes = "key.inheritedtypes";
const kind = "key.kind";
const name = "key.name";
const offset = "key.offset";
const classKind = "source.lang.swift.decl.class";

function recursiveClassesGet(json) {
	var currentClasses = [];
	if (json[kind] == classKind) {
		currentClasses.push({ name: json[name], offset: json[offset], inheritedTypes: json[inheritedTypes].map((a) => { return a[name]; }) });
	}
	if (json[substructure]) {
		return currentClasses.concat(json[substructure]
			.map((j) => recursiveClassesGet(j))
			.reduce((accumulator, currentValue) => { return accumulator.concat(currentValue); }, []));
	} else {
		return currentClasses;
	}
}

exports.analyse = analyse;
exports.recursiveClassesGet = recursiveClassesGet;
