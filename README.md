### Swift Finalizer Script

A script that should automatically add `final` to any class that can be final (i.e. isn't inherited anywhere else)

### Main inspiration

Ahmed Said's Python script that warns the user when there is a "can be final" class