var recursive = require("./swiftFilesRecursive.js");
var parser = require("./swiftFileParse.js");

function finalizableClassesWithFileData(dir) {
	return new Promise((resolve, reject) => {
		recursive.allSwiftFiles(dir).then((strings) => {
			Promise.all(strings.map((file) => {
				return parser.analyse(file).then((json) => { return parser.recursiveClassesGet(json).map((a) => { var b = a; b.file = file; return b; }); }).catch((err) => { reject(err); });
			})).then((classes) => {
				var dataAsArray = classes.reduce((acc, val) => { return acc.concat(val.map((a) => { return { name: a.name, file: a.file, offset: a.offset }; })); }, []);
				var data = new Map();
				dataAsArray.forEach((one) => { data.set(one.name, { name: one.name, file: one.file, offset: one.offset }); });
				var children = new Set(data.keys());
				var parents = new Set(classes.reduce((acc, val) => { return acc.concat(val.map((a) => a.inheritedTypes)); }, []).reduce((acc, val) => {
					return acc.concat(val);
				}, []));
				parents.forEach((one) => { children.delete(one) });
				var res = [];
				children.forEach((one) => { res.push(data.get(one)); } );
				resolve(res);
			}).catch((err) => reject(err));
		}).catch((err) => reject(err));
	});
}

function rearrangeInFiles(data) {
	var ret = new Map();
	data.forEach((one) => { 
		if (!ret.get(one.file)) {
			ret.set(one.file, []);
		}
		ret.get(one.file).push({name: one.name, offset: one.offset});
	});
	return ret;
}

exports.rearrangeInFiles = rearrangeInFiles;
exports.finalizableClassesWithFileData = finalizableClassesWithFileData;
